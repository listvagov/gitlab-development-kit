#!/usr/bin/env ruby
#
# frozen_string_literal: true

require 'socket'
require 'fileutils'

require_relative '../lib/gdk'

LOG_FILE = 'bootstrap-rails.log'

def main
  # Skip database bootstrap when in a Geo secondary
  exit if config.geo.secondary?

  postgresql = GDK::Postgresql.new
  abort 'Cannot connect to postgres' unless postgresql.ready?

  success = true

  FileUtils.rm_f(LOG_FILE)

  if postgresql.db_exists?('gitlabhq_development')
    puts 'INFO: gitlabhq_development exists, nothing to do here.'
  else
    test_gitaly_up!

    cmd = %w[support/exec-cd gitlab bundle exec rake dev:setup]

    success &&= system({ 'force' => 'yes', 'BOOTSTRAP' => '1' }, *cmd)
  end

  if config.gitlab.rails.databases.ci.__enabled && !config.gitlab.rails.databases.ci.__use_main_database
    if postgresql.db_exists?('gitlabhq_development_ci')
      puts 'INFO: gitlabhq_development_ci exists, nothing to do here.'
    else
      test_gitaly_up!

      cmd = %w[support/exec-cd gitlab bundle exec rake dev:copy_db:ci]

      success &&= system(*cmd)
    end
  end

  exit true if success

  abort "#{$PROGRAM_NAME} failed"
end

def config
  @config ||= GDK::Config.new
end

def test_gitaly_up!
  try_connect!(config.praefect? ? 'praefect' : 'gitaly')
end

def try_connect!(service)
  print "Waiting for #{service} to boot"

  sleep_time = 0.1
  repeats = 100

  repeats.times do
    sleep sleep_time
    print '.'

    begin
      UNIXSocket.new("#{service}.socket").close
      puts ' OK'

      return
    rescue Errno::ENOENT, Errno::ECONNREFUSED
    end
  end

  puts " failed to connect to #{service} after #{repeats * sleep_time}s"
  puts
  system('grep', "#{service}\.1", LOG_FILE)

  abort
end

main
